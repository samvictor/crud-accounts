from pyramid.view import view_defaults, view_config
from ..models import DBSession, User

from ..validation import validator
from .. import validation
from ..security.auth import (require_super_basic_auth,
    require_basic_auth)
from .base import DefaultView

from gevent import monkey
monkey.patch_all(select=False)

import logging
LOG = logging.getLogger(__name__)

@view_defaults(renderer="json")
class UserView(DefaultView):
    def __init__(self, request):
        self.request = request

    @view_config(route_name="account", request_method="POST")
    @validator(validation.create_account)
    @require_super_basic_auth
    def register_user(self):
        """
        .. http:post:: /account

           Creates a new account in Aurum and GCS.

           :form string username: the username of the new user.
           :form string password: the password of the new user.
           :form string email: the email of the new user. Optional.
           :reqheader Authorization: the username and password of an Aurum admin
                                     in Basic Auth.
           :status 201: when the new user is created successfully.
           :status 409: when the new username is already taken.
           :status 500: when the account cannot be created in Graphyte.

           **Example response**:

           .. sourcecode:: http

            {
                "status": "success",
                "message": "Account Fitz created",
                "data": "None"
            }
        """

        username = self.request.POST['username']
        raw_pwd = self.request.POST['password']
        email = self.request.POST.get('email', '')

        user = DBSession.query(User).filter_by(name=username).first()
        if user:
            return self.fail(409, message="Username already taken")

        resp = self.gsession.register(username, raw_pwd)

        if resp:
            user = User(username, raw_pwd)
            DBSession.add(user)
            return self.success(201,
                message="Account {} created".format(username))
        else:
            return self.error(500,
                message="Failed to create Graphyte account")
    
    
    @view_config(route_name="account", request_method="POST")
    def register_admin(self):
        """
        .. http:post:: /account

           Creates a new admin account in Aurum and GCS.

           :form string username: the username of the new user.
           :form string password: the password of the new user.
           :form string email: the email of the new user. Optional.
           :reqheader Authorization: the username and password of an Aurum admin
                                     in Basic Auth.
           :status 201: when the new user is created successfully.
           :status 409: when the new username is already taken.
           :status 500: when the account cannot be created in Graphyte.
           
           **Example response**:

           .. sourcecode:: http

            {
                "status": "success",
                "message": "Admin account Fitz created",
                "data": "None"
            }
        """

        username = self.request.POST['username']
        raw_pwd = self.request.POST['password']
        email = self.request.POST.get('email', '')

        user = DBSession.query(User).filter_by(name=username).first()
        if user:
            return self.fail(409, message="Username already taken")

		# how do you make admin accounts in Graphyte?
        resp = self.gsession.register(username, raw_pwd)

        if resp:
            user = User(username, raw_pwd, True)
            DBSession.add(user)
            return self.success(201,
                message="Admin account {} created".format(username))
        else:
            return self.error(500,
                message="Failed to create Graphyte account")
                
    
                
	# Read
	
	@view_config(request_method="GET")
	def get_info_by_name(self):
		"""
        .. http:get:: /account/(str:username)
        
        	Returns details about an account using the username
        	
           :status 200: when user found and information returned successfully.
           :status 404: when user not found.
        	
        	**Example response**:

           .. sourcecode:: http

            {
                "status": "success",
                "message": "Ok",
                "data": {
                    "name": "Sam",
                    "id": 37
                    "is_admin": False
                    "is_active": True
                }
            }
        """
	
		username = self.request.matchdict["username"]
		user = DBSession.query(User).filter_by(name=username).first()
		
		if user:
			return self.success(200,
				data={
					"name": user.name,
					"id": user.id,
					"is_admin": user.is_admin,
					"is_active": user.is_active
				})
		else:
			return self.fail(404, message="Username not found")
	
	
	@view_config(request_method="GET")
	def get_info_by_id(self):
		"""
        .. http:get:: /account/(str:id)
        
        	Returns details about an account using the user's id
        	
           :status 200: when user found and information returned successfully.
           :status 404: when user not found.
        	
        	**Example response**:

           .. sourcecode:: http

            {
                "status": "success",
                "message": "Ok",
                "data": {
                    "name": "Sam",
                    "id": 37
                    "is_admin": False
                    "is_active": True
                }
            }
        """
	
		user_id = int(self.request.matchdict["id"])
		user = DBSession.query(User).filter_by(id=user_id).first()
		
		if user:
			return self.success(200,
				data={
					"name": user.name,
					"id": user.id,
					"is_admin": user.is_admin,
					"is_active": user.is_active
				})
		else:
			return self.fail(404, message="User ID not found")
			
	
	
	# Update
	
	@view_config(route_name="account", request_method="POST")
	def update_username(self):
		"""
        .. http:post:: /account

           Changes the username of an account in Aurum.

           :form string username: the current username of the user.
           :form string new_username: the new username of the user.
           
           :status 200: when username updated succesfully
           :status 404: when user not found
           :status 409: when the new username is already taken or 
           				no new username received.
        """
	
		username = self.request.POST['username']
        new_username = self.request.POST.get('new_username', '')
		user = DBSession.query(User).filter_by(name=username).first()
		taken = DBSession.query(User).filter_by(name=new_username).first()
		
		if not new_username:
			return self.fail(409, message="No new username received")
		
		if taken:
			return self.fail(409, message="Username already taken")
		
		if user:	
			user.name = new_username
			return self.success(200,
				message="Username changed from {} to {}".format(
					username, new_username))
		else:
			return self.fail(404, message="Current username not found")
	
	
	@view_config(route_name="account", request_method="POST")
	def update_active_status(self):
		"""
        .. http:post:: /account

           Changes the "active" status of an account in Aurum.

           :form string username: the current username of the user.
           :form boolean is_active: dictates what "active" status should
                                    be changed to.
        	
           :status 200: when user found and updated successfully.
           :status 404: when user not found.
        """
	
		username = self.request.POST['username']
        is_active = self.request.POST.get("is_active")
		user = DBSession.query(User).filter_by(name=username).first()
		
		if user:	
			user.is_active = is_active
			if is_active:
				message_text = "active"
			else:
				message_text = "inactive"
				
			return self.success(200,
				message="User {} is now {}".format(
					user.name, message_text))
		else:
			return self.fail(404, message="Username not found")
	
	
	@view_config(route_name="account", request_method="POST")
	def update_admin_status(self):
		"""
        .. http:post:: /account

           Changes the "admin" status of an account in Aurum.
        	

           :form string username: the current username of the user.
           :form boolean is_admin: dictates whether user should be admin
                                   by the time the function returns.
                                   
           :status 200: when user found and updated successfully.
           :status 404: when user not found.
        """
	
		username = self.request.POST['username']
        is_admin = self.request.POST.get("is_admin")
		user = DBSession.query(User).filter_by(name=username).first()
		
		if user:	
			user.is_admin = is_admin
			if is_admin:
				message_text = "now"
			else:
				message_text = "no longer"
				
			return self.success(200,
				message="User {} is {} an administrator".format(
					user.name, message_text))
		else:
			return self.fail(400, message="Username not found")
	
	
	# Delete
	
	@view_config(route_name="account", request_method="POST")
    @require_super_basic_auth
	def delete_user(self):
		"""
        .. http:post:: /account

           Deletes a user account in Aurum.

           :form string username: the current username of the user.
           :form boolean is_admin: dictates whether user should be admin
                                   by the time the function returns.
        	
           :status 200: when user found and deleted successfully.
           :status 404: when user not found or not deleted.
           :status 500: when multiple accounts deleted
        """
		
		username = self.request.POST['username']
		user = DBSession.query(User).filter_by(name=username).first()
	
		if user:	
			accounts_deleted = DBSession.delete(user)
			if accounts_deleted == 1:
				return self.success(200,
					message="User {} has been removed from database".format(
						username))
			elif accounts_deleted == 0:
				return self.fail(404, "User {} not deleted".format(username))
			else:
				return self.error(500,
					"%d accounts deleted" % accounts_deleted)
			
		else:
			return self.fail(404, message="Username not found")
	 